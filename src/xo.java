import java.util.Scanner;

public class xo {
	static char[][] table = new char[3][3];
	static int row, col, count;
	static Scanner kb = new Scanner(System.in);

	private static void printwelcome() {
		System.out.println("welcome to xo Game");
	}

	private static void printTable() {
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table.length; j++) {
				table[i][j] = '-';
				System.out.print(table[i][j]);
			}
			System.out.println();
		}

	}

	static char player;

	private static void printTurn() {
		System.out.println("x or o :");
		Scanner kb = new Scanner(System.in);
		String n = kb.next();
		if (n.equals("x") || n.equals("X")) {
			player = 'x';
		} else {
			player = 'o';
		}

	}

	private static void input() {
		while (true) {
		System.out.println(player + " turn ");
		System.out.println("Please input Row Col :");
		Scanner kb = new Scanner(System.in);
		int row = kb.nextInt() - 1;
		int col = kb.nextInt() - 1;
		if (row > 3 || col > 3) {
			System.out.println("out of range ");
			input();
			continue;

		}
		if (table[row][col] == '-') {
			table[row][col] = player;
			Table();
			break;
		} else if (table[row][col] != '-') {
			switchplayer();
			System.out.println("this slot already taken");
			break;
		}

			
		}
		switchplayer();
		
//		printcheckRowcol();
//		if (printcheckRowcol()) {
//			System.out.println("out of range ");
//		} else {
		
//		}

	}

//	static boolean printcheckRowcol() {
//		if (!(row >= 1 && row <= 3) && !(col >= 1 && col <=3)) {
//			switchplayer();
//			return true;
//		} else {
//			return false;
//		}
//	}

	static void Table() {
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table.length; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}

	}

	private static void switchplayer() {
		if (player == 'x') {
			player = 'o';
		} else {
			player = 'x';
		}
		count++;

	}

	private static boolean checkdraw() {
		if (count == 8) {
			System.out.println("drawe!!");
			return true;
		}
		return false;
	}

	private static boolean checkwin() {
		if (table[0][0] == table[0][1] && table[0][0] == table[0][2] && table[0][0] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		if (table[1][0] == table[1][1] && table[1][0] == table[1][2] && table[1][0] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		if (table[2][0] == table[2][1] && table[2][0] == table[2][2] && table[2][0] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		if (table[0][0] == table[1][0] && table[0][0] == table[2][0] && table[0][0] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		if (table[0][1] == table[1][1] && table[0][1] == table[0][2] && table[0][1] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		if (table[0][2] == table[1][2] && table[0][2] == table[2][2] && table[0][2] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		if (table[0][0] == table[1][1] && table[0][0] == table[2][2] && table[0][0] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		if (table[2][0] == table[1][1] && table[2][0] == table[0][2] && table[2][0] != '-') {
			System.out.println("Player " + player + " win!! ");
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		printwelcome();
		printTurn();
		printTable();
		while (true) {
			input();
			if (checkwin()) {
				break;
			}else if(checkdraw()) {
				break;
			}
			//switchplayer();
		}

	}

}